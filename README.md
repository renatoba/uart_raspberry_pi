# UART

Inicie a raspberry pi, acesse via SSH execute o seguinte comando:

```
sudo raspi-config
```

Este menu irá abrir:

![](images/i1.png)

Agora escolha as seguintes opções em sequência:
- 3 (Interface options) 
- I6 (Serial port) 
- No 
- Yes 
- Ok 
- Finish 
- Yes (vai dar reboot)

## Exercício

https://gitlab.com/fse_fga/exercicios/exercicio-1-uart

